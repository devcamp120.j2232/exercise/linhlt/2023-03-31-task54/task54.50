package com.devcamp.countryregion;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CountryRegionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CountryRegionApplication.class, args);
		//khai báo array list countries và regions của VN
		ArrayList<Country> countries = new ArrayList<>();
		ArrayList<Region> regionsVietnam = new ArrayList<>();
		
		//tạo arraylist danh sách region của Việt Nam
		regionsVietnam.add(new Region("HAN", "Hanoi"));
		regionsVietnam.add(new Region("DAN", "Danang"));
		regionsVietnam.add(new Region("SGN", "Hochiminh City"));
		
		//tạo arraylist danh sách các countries
		countries.add(new Country("VN", "Vietnam", regionsVietnam));
		countries.add(new Country("USA", "United State America"));
		countries.add(new Country("AUS", "Australia"));
		
		for (Country country: countries){
			System.out.println("Country: [code = " + country.getCountryCode() + ", name= " + country.getCountryName() +"]");
			if (country.getCountryCode() == "VN"){
				for (Region region: country.getRegions()){
					System.out.println("Vietnam's region: [region code = " + region.getRegionCode() + ", region name = " + region.getRegionName() + "]");
				}
			}
		}
	}

}
